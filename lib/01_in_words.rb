    ONES = {
             0 => "zero",
             1 => "one",
             2 => "two",
             3 => "three",
             4 => "four",
             5 => "five",
             6 => "six",
             7 => "seven",
             8 => "eight",
             9 => "nine"
           }
    TEENS = {
            10 => "ten",
            11 => "eleven",
            12 => "twelve",
            13 => "thirteen",
            14 => "fourteen",
            15 => "fifteen",
            16 => "sixteen",
            17 => "seventeen",
            18 => "eighteen",
            19 => "nineteen"
          }
    TENS = {

            20 => "twenty",
            30 => "thirty",
            40 => "forty",
            50 => "fifty",
            60 => "sixty",
            70 => "seventy",
            80 => "eighty",
            90 => "ninety"
          }
    MAGNITUDE = {
            100 => "hundred",
            1_000 => "thousand",
            1_000_000 => "million",
            1_000_000_000 =>"billion",
            1_000_000_000_000 => "trillion"
        }


class Fixnum

  def in_words
    if self < 10
      return ONES[self]
    elsif self < 20
      return TEENS[self]
    elsif self < 100
      tens_word = TENS[(self / 10) * 10]
      if self % 10 != 0
        tens_word += " " + (self % 10).in_words
      else
        tens_word
      end
    else
      mag = mag_size(self)
      mag_word = (self / mag).in_words + " " + MAGNITUDE[mag]
      if (self % mag) != 0
        mag_word += " " + (self % mag).in_words
      else
        mag_word
      end

    end


  end

  def mag_size(num)
    num = num.to_s.length
    if num < 4
      return 100
    elsif num < 7
      return 1000
    elsif num < 10
      return 1_000_000
    elsif num < 13
      return 1_000_000_000
    else
      1_000_000_000_000
    end
  end


end
